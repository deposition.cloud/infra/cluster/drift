import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"
const config = new pulumi.Config()

const stack = pulumi.getStack()
const tier = pulumi.getProject()

const domain = config.require("domain")
const gitlabURL = config.require("gitlabURL")
const runnerRegistrationToken = config.requireSecret("runnerRegistrationToken")

console.log(`Booting up ${domain}...`)

const gitlabRunnerShortName = 'run'
const gitlabRunnerPrefix = 'gitlab-runner'
const gitlabRunnerNamespace = new k8s.core.v1.Namespace(gitlabRunnerPrefix, {
  metadata: { labels: { name: gitlabRunnerShortName, stack, tier } }
})

const gitlabRunner = new k8s.helm.v3.Chart(gitlabRunnerShortName, {
  repo: "gitlab",
  chart: "gitlab-runner",
  namespace: gitlabRunnerNamespace.metadata.name,
  values: {
    podLabels: { stack, tier },
    gitlabUrl: gitlabURL,
    runnerRegistrationToken,
    checkInterval: 5,
    rbac: { create: true },
    runners: {
      name: `${stack}-runner`,
      tags: 'deposition,docker',
      runUntagged: true,
      locked: false,
      privileged: true,
      podLabels: { stack, tier }
    }
  }
})
