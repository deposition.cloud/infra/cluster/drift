# Drift

> Shift away from public clouds to personal clouds

- `gitlab-runner`

## Design and Develop

``` bash
pulumi config set kubernetes:context omega # or
pulumi config set kubernetes:context live
```

Pre-requisites for Helm

``` bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
```

### Customize Stack Configuration

``` bash
pulumi config set --plaintext domain <your_domain>
pulumi config set --plaintext gitlabURL <your_gitlab_url> # e.g.: https://gitlab.mydomain.com/
pulumi config set --secret runnerRegistrationToken <your_token> # copy registration token from, e.g.: https://gitlab.com/groups/deposition.cloud/-/settings/ci_cd
```

### Deploy

``` bash
pulumi up -f -y
```

Yey! :fireworks:

## License

Ethically sourced under the [Atmosphere License](https://www.open-austin.org/atmosphere-license/)—like open source, for good.
